## Synopsis

Kuntree is a jQuery plugin for generating select lists (aka - dropdown lists) in HTML containing all of the states/provinces/territories/etc. of a given country. 

## Code Example

```javascript
$('#foo').kuntree({
	country: 'US',
	selected: 'CA'
});
```

## Motivation

This project was created due to the frustration and monotony involved in having to manually write out 50+ lines for each select list that was needed for a site for choosing a state, especially when in situations where generation the select list on the server side was less than ideal.

## Installation

```shell
npm install kuntree
```

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

To contribute, please view the included CONTRIBUTING.md for instructions on contributing to the project's code base. For extending this plugin by adding more countries, you may do the following.

1. Navigate to the root directory of the plugin and run `npm install`.
2. Create your new country definition in `[project_root]/js/data/modules/` with the format of `_[abbreviation].js`. (i.e. `_MX.js`, `_CA.js`, `_UK.js`)
3. Add all entries for the country in the newly created file in the following format.

```javascript
MX: [
	{
		title: 'States',
		items: [
			{ short: 'AL', long: 'Alabama' }
		]
	},
	{
		title: 'Federal Districts',
		items: [
			{ short: '', long: '' }
		]
	},
	{
		title: 'Territories',
		items: [
			{ short: '', long: '' }
		]
	},
	{
		title: 'Other Things',
		items: [
			{ short: 'NS', long: 'Not Sure' }
		]
	}
]
```

4. From the root directory, run `gulp` to build the new `[project_root]/dist/kuntree-full.js` and `[project_root]/dist/kuntree-full.min.js` files. Optionally, you may also run `gulp build --[country_abbreviation]` to build just for the given country abbreviation. (i.e. `gulp build --US` -> `[project_root]/dist/kuntree-US.js` && `[project_root]/dist/kuntree-US.min.js`)

## License

CC0 1.0 Universal - It's open source. Do What you want with it. :-)