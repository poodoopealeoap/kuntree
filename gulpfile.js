'use strict';
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  	maps = require('gulp-sourcemaps'),
  gulpif = require('gulp-if'),
	 del = require('del'),
	argv = require('yargs').argv,
	 src = [
		'js/_head.js',
		'js/data/_head.js',
		(Object.keys(argv).length === 2 ? 'js/data/modules/**/*.js' : 'js/data/modules/_' + Object.keys(argv)[1] + '.js'),
		'js/data/_foot.js',
		'js/modules/**/*.js',
		'js/_foot.js'
	 ];

gulp.task('build:min', ['build'], function() {
	return gulp.src('dist/kuntree-' + (Object.keys(argv).length === 2 ? 'full' : Object.keys(argv)[1]) + '.js')
		.pipe(uglify())
		.pipe(rename('kuntree-' + (Object.keys(argv).length === 2 ? 'full' : Object.keys(argv)[1]) + '.min.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('build', function() {
	return gulp.src(src)
		.pipe(maps.init())
		.pipe(concat('kuntree-' + (Object.keys(argv).length === 2 ? 'full' : Object.keys(argv)[1]) + '.js'))
		.pipe(maps.write('./'))
		.pipe(gulp.dest('dist'));
});

gulp.task('clean', function() {
	del('dist');
});

gulp.task('default', ['all']);