function processOpts(opts) {
	var defaults = {
		selected: null,
		country: 'US'
	};
	opts = typeof(opts) === 'string' ? { country: opts } : typeof(opts) === 'object' ? opts = opts : opts = defaults;
	opts.selected = opts.selected ? opts.selected : defaults.selected;
	opts.country = opts.country ? opts.country : defaults.country;
	return opts;
};