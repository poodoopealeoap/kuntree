function buildDropdown($this, opts) {
	var content = '';
	for (var i = 0; i < data[opts.country].length; i++) {
		content += '<optgroup label="' + data[opts.country][i].title + '">';
		for (var j = 0; j < data[opts.country][i].items.length; j++) {
			content += '<option value="' + data[opts.country][i].items[j].short + '">' + data[opts.country][i].items[j].long + '</option>';
		}
		content += '</optgroup>';
	}
	$this.html(content);
	$this.val(opts.selected);
}